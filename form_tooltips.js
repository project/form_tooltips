
//How far should tool tip box appear from the mouse?
var offsetX = 20;
var offsetY = 10;

//When document is finished process tooltipping functionality
$(document).ready(function() {
  //hide all classes that we want tooltipped
  $('.standard>.form-item,.standard .fieldset-wrapper>.form-item').find('.description').hide();
  $('.content-multiple-table').next('.description').hide();
    $('.standard>.form-item,.standard .fieldset-wrapper>.form-item,.content-multiple-table').hover(function(e){
        //sometimes the description is a child of the element
        if (form_tooltipsProcess($(this))=='child') {
        var helptext = form_tooltipsFormtextchild($(this));
        form_tooltipsAppend(helptext,e);
      }
      //sometimes the description is a sibling
      else if (form_tooltipsProcess($(this))=='sibling') {
        var helptext = form_tooltipsFormtextsibling($(this));
        form_tooltipsAppend(helptext,e);
      }
    }, function() {
      $('#desctip').remove();
    });
  //Make tooltip follow mouse (can impact performance)
  $(this).mousemove(function(e) {    $("#desctip").css('top', e.pageY + offsetY).css('left', e.pageX + offsetX);
    });
});

//decide if there is a element to tooltipify, and if so is it child or sibling?
function form_tooltipsProcess(item) {
  if (item.find('.description').html()){
    return 'child';
  }
  if (item.next('.description').html()){
    return 'sibling';
  }
  else {
    return false;
  }
}

//Format HTML to go in tip
function form_tooltipsFormtextchild(item) {
  var helptext = item.find('.description').html();
  return helptext;
}

function form_tooltipsFormtextsibling(item) {
  var helptext = item.next('.description').html();
  return helptext;
}

//Append text to body (absolutely positioned)
function form_tooltipsAppend(text,e) {
  $('<div id="desctip">' + text + '</div>')
  .css('top', e.pageY + offsetY)
  .css('left', e.pageX + offsetX)
  .appendTo('body');
}
